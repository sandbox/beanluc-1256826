DESCRIPTION
-----------

Drupal 7 as Mindtouch authentication provider:
Mindtouch has an authentication provider for Drupal 6 built in.
But Drupal 7 is not compatible with Mindtouch's built-in Drupal
authentication provider. this module implements a custom remote
authentication service which Mindtouch can connect to, so that users
of a Drupal 7 website can use their Drupal credential to log into a
Mindtouch website on the same domain.

AUTHORSHIP
----------
Bean Lucas
bean@lucas.net

INSTALLATION
------------

Copy mindtouch_remote_auth directory and contents to your modules directory.
Enable on the admin modules page.

Configure Mindtouch to use Drupal 7 as an authentication provider:
Do that in Mindtouch's Control panel, under System Settings,
Authentication, Add Authentication Service. Under "Choose An
Authentication Provider", choose "Custom", tick the radio button
marked "Remote", and enter in the URI field:
http://<drupal7host>/?q=authentication/mindtouch